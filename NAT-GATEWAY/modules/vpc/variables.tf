variable "prefix_name" {}
variable "vpc_cidr_block" {}
variable "public-subnet" {}
variable "private-subnet" {}
variable "availability_zone" {}
variable "nat-instance_id" {}

