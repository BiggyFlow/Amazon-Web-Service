output "vpc_id" {
    value = aws_vpc.vpc_main.id
}
output "public-subnet_id" {
    value = aws_subnet.public-subnet.id
}
output "private-subnet_id" {
    value = aws_subnet.private-subnet.id
}
