resource "aws_vpc" "vpc_main" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "${var.prefix_name}-VPC"
  }
}

resource "aws_subnet" "public-subnet" {
  vpc_id            = aws_vpc.vpc_main.id
  cidr_block        = var.public-subnet
  availability_zone = var.availability_zone
  tags = {
    Name = "${var.prefix_name}-public-subnet"
  }
}

resource "aws_subnet" "private-subnet" {
  vpc_id            = aws_vpc.vpc_main.id
  cidr_block        = var.private-subnet
  availability_zone = var.availability_zone
  tags = {
    Name = "${var.prefix_name}-private-subnet"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc_main.id
  tags = {
    Name = "${var.prefix_name}-igw"
  }
}

resource "aws_route_table" "public-rt" {
  vpc_id = aws_vpc.vpc_main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = "${var.prefix_name}-public-subnet-rt"
  }
}

resource "aws_route_table" "private-rt" {
  vpc_id = aws_vpc.vpc_main.id
  route {
    cidr_block = "0.0.0.0/0"
    network_interface_id = var.nat-instance_id
  }
  tags = {
    Name = "${var.prefix_name}-private-subnet-rt"
  }
}

resource "aws_route_table_association" "public-subnet" {
  subnet_id      = aws_subnet.public-subnet.id
  route_table_id = aws_route_table.public-rt.id
}
resource "aws_route_table_association" "private-subnet" {
  subnet_id      = aws_subnet.private-subnet.id
  route_table_id = aws_route_table.private-rt.id
}

