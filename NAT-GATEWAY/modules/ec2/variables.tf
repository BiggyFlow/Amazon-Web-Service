variable "prefix_name" {}
variable "ami" {}
variable "ami_nat" {}
variable "key_name" {}
variable "vpc_id" {}
variable "vpc_cidr_block" {}
variable "public-subnet" {}
variable "public-subnet_cidr_block" {}
variable "private-subnet_cidr_block" {}
variable "private-subnet" {}
variable "instance_type" {}
variable "associate_public_ip_address" {}
variable "http_port" {}
variable "https_port" {}
variable "ssh_port" {}

