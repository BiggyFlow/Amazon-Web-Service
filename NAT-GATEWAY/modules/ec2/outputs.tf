output "nat-instance_id" {
    value = aws_instance.nat-instance.primary_network_interface_id
}