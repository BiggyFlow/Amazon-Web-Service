resource "aws_instance" "server-public-subnet" {
  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = var.key_name
  vpc_security_group_ids      = ["${aws_security_group.ssh-sg.id}"]
  subnet_id                   = var.public-subnet
  associate_public_ip_address = var.associate_public_ip_address
  tags = {
    Name        = "${var.prefix_name}-server-public-subnet"
  }
}

resource "aws_instance" "server-private-subnet" {
  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = var.key_name
  vpc_security_group_ids      = ["${aws_security_group.ssh-sg.id}"]
  subnet_id                   = var.private-subnet
  tags = {
    Name        = "${var.prefix_name}-server-private-subnet"
  }
}

resource "aws_security_group" "ssh-sg" {
  description = "instance nat security group"
  name        = "${var.prefix_name}-ssh-sg"
  vpc_id      = var.vpc_id
  ingress {
    from_port   = var.ssh_port
    to_port     = var.ssh_port
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name        = "${var.prefix_name}-ssh-sg"
  }
}



resource "aws_instance" "nat-instance" {
  ami                         = var.ami_nat
  instance_type               = var.instance_type
  key_name                    = var.key_name
  source_dest_check = false 
  vpc_security_group_ids      = ["${aws_security_group.nat-sg.id}"]
  subnet_id                   = var.public-subnet
  associate_public_ip_address = var.associate_public_ip_address
  tags = {
    Name        = "${var.prefix_name}-nat-instance"
  }
}

resource "aws_security_group" "nat-sg" {
  description = "instance nat security group"
  name        = "${var.prefix_name}-nat-sg"
  vpc_id      = var.vpc_id
  ingress {
    from_port   = var.http_port
    to_port     = var.http_port
    protocol    = "tcp"
    cidr_blocks = [var.private-subnet_cidr_block]
  }
  ingress {
    from_port   = var.https_port
    to_port     = var.https_port
    protocol    = "tcp"
    cidr_blocks = [var.private-subnet_cidr_block]
  }
  ingress {
    from_port   = var.ssh_port
    to_port     = var.ssh_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = var.http_port
    to_port     = var.http_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = var.https_port
    to_port     = var.https_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name        = "${var.prefix_name}-nat-sg"
  }
}

