variable "prefix_name" {
    description = "prefix of your project or organization"
}
variable "vpc_cidr_block" {}
variable "public-subnet" {}
variable "private-subnet" {}
variable "availability_zone" {
    description = "availability zone for instance"
}
variable "associate_public_ip_address" {
    description = "false or true, if pulibc must be attach to the instance"
}
variable "http_port" {
    description = "the port number that nat wiil accept to forward"
}
variable "https_port" {
    description = "the port number that nat wiil accept to forward"
}
variable "ssh_port" {
    description = "the ssh port"
}
variable "instance_type" {}
variable "key_name" {}
variable "ami" {}
variable "ami_nat" {
    description = "ami with nat already configured"
}
