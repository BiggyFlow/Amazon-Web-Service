module "ec2" {
  source                      = "./modules/ec2"
  prefix_name                 = var.prefix_name
  public-subnet               = module.vpc.public-subnet_id
  public-subnet_cidr_block    = var.public-subnet
  private-subnet_cidr_block   = var.private-subnet
  private-subnet              = module.vpc.private-subnet_id
  ami                         = var.ami
  ami_nat                     = var.ami_nat
  key_name                    = var.key_name
  instance_type               = var.instance_type
  associate_public_ip_address = var.associate_public_ip_address
  http_port                   = var.http_port
  https_port                  = var.https_port
  ssh_port                    = var.ssh_port
  vpc_id                      = module.vpc.vpc_id
  vpc_cidr_block              = var.vpc_cidr_block
}

module "vpc" {
  source            = "./modules/vpc"
  prefix_name       = var.prefix_name
  vpc_cidr_block    = var.vpc_cidr_block
  public-subnet     = var.public-subnet
  private-subnet    = var.private-subnet
  availability_zone = var.availability_zone
  nat-instance_id   = module.ec2.nat-instance_id
}
