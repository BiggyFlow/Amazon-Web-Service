L'objectif ici est de mettre en place une nat-gateway. 

- Pour que ça fonctionne, vus devez utiliser une ami qui a déjà une configuration nat. Vous en trouverez plein sur l'AMI Catalog -> Community AMIs.
J'ai utilisé l'ami nat suivant "amzn-ami-vpc-nat-2018.03.0.20221018.0-x86_64-ebs"

Un serveur sur le réseau public pour vous permettre une connexion en ssh sur le servveur qui sera dans le réseau privé.
Un serveur sur le réseau privé qui enverra toutes les demandes internet vers l'intance nat. 

Enjoy ! 
