output "instance_webservers-a" {
  value = module.ec2.webservers-a
}
output "instance_webservers-b" {
  value = module.ec2.webservers-b
}

output "webservers-a_private_ip" {
  value = module.ec2.webservers-a_private_ip
}

output "webservers-b_private_ip" {
  value = module.ec2.webservers-b_private_ip
}

output "dns_name" {
  value = module.lb.dns_name 
}
