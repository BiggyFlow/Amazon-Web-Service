output "webservers-a" {
    value = aws_instance.webservers-a.id
}
output "webservers-b" {
    value = aws_instance.webservers-b.id
}

output "webservers-a_private_ip" {
    value = aws_instance.webservers-a.private_ip
}

output "webservers-b_private_ip" {
    value = aws_instance.webservers-b.private_ip
}

