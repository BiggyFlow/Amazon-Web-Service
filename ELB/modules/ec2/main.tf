resource "aws_instance" "webservers-a" {
  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = var.key_name
  vpc_security_group_ids      = ["${aws_security_group.allow_www.id}"]
  subnet_id                   = var.subnet_id-a
  associate_public_ip_address = var.associate_public_ip_address
  tags = {
    Environment = "${var.prefix_name}-dev"
    Name        = "${var.prefix_name}-webservers-a"
  }
}

resource "aws_instance" "webservers-b" {
  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = var.key_name
  vpc_security_group_ids      = ["${aws_security_group.allow_www.id}"]
  subnet_id                   = var.subnet_id-b
  associate_public_ip_address = var.associate_public_ip_address
  tags = {
    Environment = "${var.prefix_name}-dev"
    Name        = "${var.prefix_name}-webservers-b"
  }
}

resource "aws_security_group" "allow_www" {
  description = "security group allow http, ssh and icmp from vpc cidr"
  name        = "${var.prefix_name}-allow_www"
  vpc_id      = var.vpc_id
  ingress {
    from_port   = var.http_port
    to_port     = var.http_port
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }
  ingress {
    from_port   = var.ssh_port
    to_port     = var.ssh_port
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = [var.vpc_cidr_block]
  }
  tags = {
    Name        = "${var.prefix_name}-http_ec2"
  }
}

