output "vpc_id" {
    value = aws_vpc.vpc_main.id 
}

output "subnet_webservers-a" {
    value = aws_subnet.webservers-a.id
}

output "subnet_webservers-b" {
    value = aws_subnet.webservers-b.id
}