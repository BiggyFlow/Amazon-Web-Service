resource "aws_vpc" "vpc_main" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "${var.prefix_name}-VPC"
  }
}

resource "aws_subnet" "webservers-a" {
  vpc_id            = aws_vpc.vpc_main.id
  cidr_block        = var.subnet-a
  availability_zone = var.az-a
  tags = {
    Name = "${var.prefix_name}-subnet-webservers-a"
  }
}

resource "aws_subnet" "webservers-b" {
  vpc_id            = aws_vpc.vpc_main.id
  cidr_block        = var.subnet-b
  availability_zone = var.az-b
  tags = {
    Name = "${var.prefix_name}-subnet-webservers-b"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc_main.id
  tags = {
    Name = "${var.prefix_name}-igw"
  }
}

resource "aws_route_table" "rt" {
  vpc_id = aws_vpc.vpc_main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = "${var.prefix_name}-rt"
  }
}

resource "aws_route_table_association" "subnet-a" {
  subnet_id      = aws_subnet.webservers-a.id
  route_table_id = aws_route_table.rt.id
}
resource "aws_route_table_association" "subnet-b" {
  subnet_id      = aws_subnet.webservers-b.id
  route_table_id = aws_route_table.rt.id
}

