resource "aws_db_subnet_group" "rds_subnet" {
    name = "${var.prefix_name}-rds_subnet_grpp"
    subnet_ids = ["${var.subnets-a}", "${var.subnets-b}"]
}

resource "aws_db_instance" "myawsdb" {
  allocated_storage    = 20
  engine               = "mariadb"
  engine_version       = "10.6.11"
  instance_class       = "db.t2.micro"
  db_name                 = "myawsdb"
  username             = "username"
  password             = "Pa$$w0rd"
  db_subnet_group_name = "${aws_db_subnet_group.rds_subnet.id}"
  skip_final_snapshot = true
  multi_az = false
  final_snapshot_identifier = "myawsdbfinalsnap"
  vpc_security_group_ids = ["${aws_security_group.sg_rds.id}"]
}

resource "aws_security_group" "sg_rds" {
  name = "${var.prefix_name}-sg_rds"
  vpc_id = var.vpc_id
  ingress {
    from_port = "${var.db_port}"
    to_port   = "${var.db_port}"
    protocol  = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }
  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = [var.vpc_cidr_block]
  }
  lifecycle {
    create_before_destroy = true
  }
}