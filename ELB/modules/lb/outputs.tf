output "dns_name" {
  value = aws_lb.http-lb.dns_name
}

output "public_ip_allow" {
  value = aws_security_group.lb_sg.ingress.*.cidr_blocks
}
