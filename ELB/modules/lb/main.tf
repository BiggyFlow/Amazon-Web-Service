resource "aws_security_group" "lb_sg" {
  name        = "${var.prefix_name}-lb_security_group"
  description = "Security group for load balancer"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.public_ip_allow]
  }
  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.prefix_name}-allow_http_elb"
  }
}

resource "aws_lb" "http-lb" {
  name               = "${var.prefix_name}-http-lb"
  load_balancer_type = var.load_balancer_type
  subnets            = ["${var.subnets-a}", "${var.subnets-b}"]
  security_groups    = ["${aws_security_group.lb_sg.id}"]
  internal           = false
}
resource "aws_lb_target_group" "http-lb-tg" {
  name        = "${var.prefix_name}-http-lb-tg"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = var.target_type
}

resource "aws_lb_target_group_attachment" "instances-a" {
  target_group_arn = aws_lb_target_group.http-lb-tg.id
  target_id        = var.instances-a
  port             = 80
}
resource "aws_lb_target_group_attachment" "instances-b" {
  target_group_arn = aws_lb_target_group.http-lb-tg.id
  target_id        = var.instances-b
  port             = 80
}

resource "aws_lb_listener" "for-http-lb" {
  load_balancer_arn = aws_lb.http-lb.arn
  port              = "80"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.http-lb-tg.arn
  }
}

