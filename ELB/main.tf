provider "aws" {
  region = "us-east-1"
}

module "ec2" {
  source                      = "./modules/ec2"
  prefix_name                 = var.prefix_name
  vpc_id                      = module.vpc.vpc_id
  vpc_cidr_block              = var.vpc_cidr_block
  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = var.key_name
  subnet_id-a                 = module.vpc.subnet_webservers-a
  subnet_id-b                 = module.vpc.subnet_webservers-b
  associate_public_ip_address = var.associate_public_ip_address
  http_port                   = var.http_port
  ssh_port                    = var.ssh_port
  public_ip_allow             = var.public_ip_allow
}

module "lb" {
  source             = "./modules/lb"
  prefix_name        = var.prefix_name
  public_ip_allow    = var.public_ip_allow
  subnets-a          = module.vpc.subnet_webservers-a
  subnets-b          = module.vpc.subnet_webservers-b
  vpc_id             = module.vpc.vpc_id
  lb_port            = var.lb_port
  instances-a        = module.ec2.webservers-a
  instances-b        = module.ec2.webservers-b
  load_balancer_type = var.load_balancer_type
  target_type        = var.target_type
}

module "rds" {
  source          = "./modules/rds"
  prefix_name     = var.prefix_name
  vpc_id          = module.vpc.vpc_id
  subnets-a       = module.vpc.subnet_webservers-a
  subnets-b       = module.vpc.subnet_webservers-b
  vpc_cidr_block = var.vpc_cidr_block
  db_port         = var.db_port

}

module "vpc" {
  source         = "./modules/vpc"
  prefix_name    = var.prefix_name
  vpc_cidr_block = var.vpc_cidr_block
  subnet-a       = var.subnet-a
  subnet-b       = var.subnet-b
  az-a           = var.az-a
  az-b           = var.az-b

}

