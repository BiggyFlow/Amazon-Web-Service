variable "prefix_name" {
  description = "le prefix de ma societe"
}

variable "public_ip_allow" {
  description = "l'adresse ip publique qui peut accéder au load balancer"
}

variable "vpc_cidr_block" {}
variable "subnet-a" {}
variable "az-a" {}
variable "az-b" {}
variable "subnet-b" {}
variable "key_name" {}
variable "ami" {}
variable "instance_type" {}


variable "associate_public_ip_address" {}
variable "lb_port" {}
variable "db_port" {}
variable "http_port" {}
variable "ssh_port" {}
variable "load_balancer_type" {}

variable "target_type" {}




