terraform {
  cloud {
    organization = "xx.xxx.xxx.xxx" # put your organization here 
    workspaces {
      name = "xx.xx.xx.xx" # put your workspace here
    }
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.57.1"
    }
  }
}
